// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBXCCybliSvrW32Bd231EphcXdi4z5JqP0',
    authDomain: 'fir-577c4.firebaseapp.com',
    databaseURL: 'https://fir-577c4.firebaseio.com',
    projectId: 'fir-577c4',
    storageBucket: 'fir-577c4.appspot.com',
    messagingSenderId: '162951524614',
    appId: '1:162951524614:web:6a8678c5f82e38d8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
