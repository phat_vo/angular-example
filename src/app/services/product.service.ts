import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private firestore: AngularFirestore) { }

  getProducts() {
    return this.firestore.collection('products').snapshotChanges();
  }

  createProduct(product: Product) {
    return this.firestore.collection('products').add(product);
  }

  updateProduct(product: Product) {
    this.firestore.doc('products/' + product.id).update(product);
  }

  getProductById(productId: string) {
    return this.firestore.collection('products').doc(productId).valueChanges();
  }

  deleteProduct(productId: string) {
    this.firestore.doc('products/' + productId).delete();
  }
}
