import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

/// Notify users about errors and other helpful stuff
export interface Msg {
    content: string;
    style: string;
}

@Injectable()
export class NotifyService {
    private message = new Subject<Msg | null>();

    msg = this.message.asObservable();

    update(content: string, style: 'error' | 'info' | 'success') {
        const msg: Msg = { content, style };
        this.message.next(msg);
    }

    clear() {
        this.message.next(null);
    }
}
