export class Product {
    id: string;
    productName: string;
    description: string;
    price: number;
    image: string
}
