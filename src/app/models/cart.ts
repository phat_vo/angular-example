export class Cart {
    id: string;
    productName: string;
    quantity: number;
    price: number;
}
