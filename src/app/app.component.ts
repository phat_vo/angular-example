import { Component } from '@angular/core';
import {Cart} from './models/cart';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  countCart: Observable<number>;
  constructor(private store: Store<{ cart: Cart[] }>) {
    this.countCart = store.pipe(select('cart'));
  }
}
