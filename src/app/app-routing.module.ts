import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { ProductsComponent as AdminProductsComponent} from './admin/products/products.component';
import { ProductAddComponent } from './admin/products/product-add/product-add.component';
import { ProductUpdateComponent } from './admin/products/product-update/product-update.component';
import { UsersComponent as AdminUsersComponent } from './admin/users/users.component';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';
import { UserComponent } from './components/user/user.component';
import { UserFormComponent } from './components/user/user-form/user-form.component';

const routes: Routes = [
  { path: '', component: ProductsComponent },
  {
    path: 'products', component: ProductsComponent,
    children: [
      { path: 'cart', component: CartComponent }]
  },
  { path: 'cart', component: CartComponent },
  {
    path: 'user', component: UserComponent,
    children: [
      { path: '', component: UserFormComponent },
    ]
  },
  {
    path: 'admin', component: AdminComponent,
    children: [
      { path: '', component: AdminProductsComponent },
      { path: 'products', component: AdminProductsComponent },
      { path: 'product/add', component: ProductAddComponent },
      { path: 'product/update/:id', component: ProductUpdateComponent },
      { path: 'users', component: AdminUsersComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
