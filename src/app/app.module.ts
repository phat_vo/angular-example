import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AdminComponent } from './admin/admin.component';
import { UsersComponent as AdminUsersComponent } from './admin/users/users.component';
import { ProductsComponent as AdminProductsComponent } from './admin/products/products.component';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';
import { UserComponent } from './components/user/user.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { UserFormComponent } from './components/user/user-form/user-form.component';
import { StoreModule } from '@ngrx/store';
import { CartReducer } from './components/cart/cart.reducers';
import { ProductAddComponent } from './admin/products/product-add/product-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductUpdateComponent } from './admin/products/product-update/product-update.component';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    StoreModule.forRoot({ cart: CartReducer }),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    AdminComponent,
    AdminUsersComponent,
    AdminProductsComponent,
    UserComponent,
    ProductsComponent,
    CartComponent,
    UserFormComponent,
    ProductAddComponent,
    ProductUpdateComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
