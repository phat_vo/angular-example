import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.scss']
})

export class ProductUpdateComponent implements OnInit {
  productUpdateForm = new FormGroup({
    productName: new FormControl(''),
    description: new FormControl(''),
    price: new FormControl(''),
  });

  id: string;

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.productService.getProductById(params.id).subscribe( res => {
        this.id = params.id;
        this.productUpdateForm.patchValue({ ...res});
      });
    });
  }

  onUpdateProduct() {
    this.productService.updateProduct({ ...this.productUpdateForm.value, id: this.id});
  }
}
