import { Component} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})

export class ProductAddComponent {
  productForm = new FormGroup({
    productName: new FormControl(''),
    description: new FormControl(''),
    price: new FormControl(''),
  });

  constructor(private productService: ProductService) { }

  onSubmit() {
    const avartar = 'https://firebasestorage.googleapis.com/v0/b/fir-577c4.appspot.com/o/images%2Fmacbook-select-space-gray-201706.jpeg?alt=media&token=87809a93-705c-4459-b1fd-c0a56cb19749';
    this.productService.createProduct({ ...this.productForm.value, image: avartar});
  }
}
