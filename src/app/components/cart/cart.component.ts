import {Component} from '@angular/core';
import {Cart} from '../../models/cart';
import {Observable} from 'rxjs';
import { select, Store, State} from '@ngrx/store';
import {sumBy} from 'lodash';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {
  carts: Observable<Cart[]>;
  total: number;
  constructor(private store: Store<{ cart: Cart[] }>, private state: State<'cart'>) {
    this.carts = store.pipe(select('cart'));
    this.total = sumBy(this.state.getValue().cart, 'total');
  }
}
