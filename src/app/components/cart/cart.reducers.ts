import {ActionEx, CartActionTypes} from './cart.actions';
import { groupBy, map, keys, reduce} from 'lodash';
export const initialState = [];
export function CartReducer(state = initialState, action: ActionEx) {
  switch (action.type) {
    case CartActionTypes.Add:
      let group = groupBy([...state, action.payload], 'id');
      console.log(group)
      let result = map(keys(group), (e) => {
        return reduce(group[e], (r, o) => {
          return r.total += +o.total, r.quantity += +o.quantity, r;
        }, {
          id: e,
          total: 0,
          quantity: 0,
          productName: group[e][0].productName,
          price: group[e][0].price
        });
      });
      return result;
    case CartActionTypes.Remove:
      return [
        ...state.slice(0, action.payload),
        ...state.slice(action.payload + 1)
      ];
    default:
      return state;
  }
}