import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {Cart} from '../../models/cart';
import {CartAdd} from '../../components/cart/cart.actions';
import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[];

  constructor(private productService: ProductService, private store: Store<{ cart: Cart[] }>) {}
  
  ngOnInit() {
    this.productService.getProducts().subscribe(data => {
      this.products = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as Product;
      });
    });
  }

  AddCart(product) {
    const newCart = {
      id: product.id,
      productName: product.productName,
      quantity: 1,
      total: product.price,
      price: product.price
    };
    this.store.dispatch(new CartAdd(newCart));
  }
}
